# MY 15BAY DAS BUILD

This article has been based on the following articles.

https://www.serverbuilds.net/16-bay-das
https://www.reddit.com/r/DataHoarder/comments/89jqfu/my_300_diy_16_drive_das_build/
https://www.reddit.com/r/PleX/comments/6a8n7o/plex_build_recommendation_direct_attached_storage/

Most of these products are available in the USA. I live in Europe and shipping these products are staggering. I still wanted to build a DAS, so I decided to find similar products in Europe. This is my quest sofar.

Chassis:
[Zalman MS800](https://maxict.nl/zalman-ms800-computerbehuizing-midi-toren-zwart-p3478813.html) 		1x €59,68
This chassis has 10x 5.25" slots. It is more effecient to get 3 HDD bays which fit in 2x 5.25" slots instead of 4 HDD bays which fit in 3x 5.25" slots. I chose to go for the 15 disk setup.

HDD Bay:
[Icy Dock FatCage MB153SP-B (link - buy 5)](https://www.amazon.de/dp/B009WDNL70/)

Main Components:
[LSI 9201-16e External SAS HBA (link)](https://www.ebay.nl/itm/LSI-SAS-9201-16e-6Gb-s-SATA-SAS-PCI-E-x8-HBA-Controller-Full-Profile/153690882191)
[LSI 9201-16e External SAS HBA (link)](https://www.ebay.nl/itm/LSI-9201-16e-6Gbps-16-lane-external-SAS-HBA-P20-IT-Mode-ZFS-FreeNAS-unRAID/153291384883)
You really have to search the internet to buy this card. The prices range until €500.

PSU:
[EVGA 750 N1 750W power supply (link)](https://maxict.nl/evga-110-bq-0750-v2-750w-atx-zwart-power-supply-unit-p7230428.html)            1x €89,15
You could also go for the 500W. Should be more than sufficient. With this option you could add a mainboard in the future. This powersupply comes with the 'PSU Tester Adapter'.

Cables:
[4 Pin Molex to 2x SATA Power Splitter (link)](https://www.amazon.de/dp/B000KDQSMQ/)

[SFF-8088 to 4xSATA breakout cable (link - buy 4)](https://www.amazon.de/dp/B07M8F1MV7/)

[2-pack PWM to Molex adapter cable (link)](https://www.amazon.de/dp/B07FHYMWX4/)

Optional: [24-pin PSU tester / short / jumper - start up switch (link)](https://www.amazon.de/dp/B004L26ZHU/)

Cooling:
[Arctic BioniX P120 120mm PWM PST (link - buy 6)](https://www.amazon.de/gp/product/B07ZJF9L7H/)

[Arctic F12 120mm PWM PST 5-pack (link)](https://www.amazon.de/dp/B00NTUJTAK/)
[Arctic F8 80mm PWM PST 5-pack (link)](https://www.amazon.de/dp/B00NTUJG62/)


Pricing
DAS build outline


https://www.amazon.de/dp/B07RMHH43W
